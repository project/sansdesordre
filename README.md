
## Installation

```
composer require drupal/sansdesordre;
drush cr;
drush en sansdesordre -y
```

## Why should I use this module?
The SansDésordre module provides easier editing menu links for non
administrative roles.

Only menu links that belong to the menu name this link is a part of will be
displayed in the dropdown select for add or edit link.

Other UX/UI improvements for scalability and or ease of use will be evaluated
for addition to this module, feel free to submit patch for future improvements.
Currently if you wish to disable this functionality by role you can do this by
the permissions.

## Note:
Currently Administrator role always has this functionality enabled you
cannot disable this functionality for administrator roles without disabling
this module however other roles you can disable this functionality per role.

