<?php

namespace Drupal\sansdesordre;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides utility functions for the Sansdesordre module.
 */
class Utils {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Utils object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Retrieves the current route name.
   *
   * @return string
   *   The name of the current route.
   */
  public function getRouteName() {
    $route = $this->routeMatch->getRouteName();

    return $route;
  }

  /**
   * Retrieves the menu link entity based on the provided menu link ID (mlid).
   *
   * @param int $mlid
   *   The ID of the menu link.
   *
   * @return \Drupal\menu_link_content\Entity\MenuLinkContentInterface|null
   *   The menu link entity if found, or null if not found.
   */
  public function getMenuLinkFromMlid($mlid) {
    $menuLink = $this->entityTypeManager->getStorage('menu_link_content')->load($mlid);
    return $menuLink;
  }

}
